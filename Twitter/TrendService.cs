﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;

namespace Twitter
{
    class TrendService
    {
        private TwitterService service;

        public TrendService(TwitterService service) { this.service = service; }

        public void GetTrends()
        {
            var trends = service.ListLocalTrendsFor(new ListLocalTrendsForOptions() { Id = 1 }).Take(10);

            foreach (var trend in trends)
                Repository.GetInstanse().AddTrend(trend);
        }
    }
}
