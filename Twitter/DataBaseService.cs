﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter
{
    class DataBaseService
    {
        private const string connect = @"Data Source=DESKTOP-03AU94S\SQLEXPRESS;Initial Catalog=twitter;Integrated Security=True";

        public void AddTweetDB(string author, string screen_name, DateTime date, string text, string image)
        {
            using (var connection = new SqlConnection(connect))
            {
                connection.Open();
                var command = new SqlCommand("INSERT INTO tweets (author, screen_name, date, text, image) VALUES (@author, @screen_name, @date, @text, @image)", connection);
                command.Parameters.Add(new SqlParameter("@text", text));
                command.Parameters.Add(new SqlParameter("@image", image));
                command.Parameters.Add(new SqlParameter("@author", author));
                command.Parameters.Add(new SqlParameter("@screen_name", screen_name));
                command.Parameters.Add(new SqlParameter("@date", date));
                command.ExecuteNonQuery();

            }
        }

        public void DeleteTweetDB()
        {
            using (var connection = new SqlConnection(connect))
            {
                connection.Open();
                var command = new SqlCommand("truncate table tweets", connection);
                command.ExecuteNonQuery();
            }
        }

        public void GetTweetDb()
        {
            using (var connection = new SqlConnection(connect))
            {
                connection.Open();
                var command = new SqlCommand("SELECT * FROM tweets", connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {

                    var text = reader["text"].ToString();
                    var image = reader["image"].ToString();
                    var author = reader["author"].ToString();
                    var screen_name = reader["screen_name"].ToString();
                    var date = (DateTime) reader["date"];
                    var tweet = new Tweet(text, image, author, screen_name, date);
                    Repository.GetInstanse().AddTweet(tweet);
                }
            }
        }
    }
}
