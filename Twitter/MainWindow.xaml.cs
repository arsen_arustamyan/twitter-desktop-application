﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TweetSharp;

namespace Twitter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private OAuthRequestToken requestToken;
        public static TwitterService twitterService;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenAutorizePage()
        {
            const string consumerKey = "";
            const string consumerSecret = "";

            twitterService = new TwitterService(consumerKey, consumerSecret);
            requestToken = twitterService.GetRequestToken();
            var uri = twitterService.GetAuthorizationUri(requestToken);

            browser.Navigate(uri);
        }

        private void browser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            if (e.Uri.ToString() == "https://api.twitter.com/oauth/authorize")
            {
                var verifier = GetVerifierFromPage();

                var accessToken = twitterService.GetAccessToken(requestToken, verifier);

                twitterService.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);

                var twitterInterface = new TwitterInterface();

                twitterInterface.Show();

                Close();
            }
        }


        private string GetVerifierFromPage()

        {
            dynamic doc = this.browser.Document;

            var html = doc.documentElement.innerHtml;

            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(html);

            var codeNode = htmlDoc.DocumentNode.SelectSingleNode("//code");

            return codeNode.InnerText;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OpenAutorizePage();
        }
    }
}
