﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;

namespace Twitter
{
    class Tweet
    {
        public string text { get; set; }
        public string image { get; set; }
        public string author { get; set; }
        public string screen_name { get; set; }
        public DateTime date { get; set; }

        public string substractDate { get; set; }

        public Tweet(string text, string image, string author,
                     string screen_name, DateTime date)
        {
            this.text = text;
            this.image = image;
            this.author = author;
            this.screen_name = screen_name;
            this.date = date;
        }
    }
}
