﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TweetSharp;
using System.Data.SqlClient;

namespace Twitter
{
    class CachingService
    {
        private DataBaseService db;

        public CachingService()
        {
            this.db = new DataBaseService();
        }

        private List<string> images = new List<string>();

        private void SaveUserImage()
        {
            try
            {
                for (var i = 0; i < Repository.GetInstanse().GetTweets.Count; i++)
                {
                    using (var webClient = new WebClient())
                    {
                        var url = Repository.GetInstanse().GetTweets[i].image;
                        var imagePath = Repository.GetInstanse().GetTweets[i].author + Path.GetExtension(url);
                        images.Add(imagePath);
                        webClient.DownloadFile(url, imagePath);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void SaveTweetDb()
        //{
        //    for (var i = 0; i < images.Count; i++)
        //    {
        //        db.AddTweetDB(Repository.GetInstanse().GetTweets[i].author, Repository.GetInstanse().GetTweets[i].screen_name,
        //                      Repository.GetInstanse().GetTweets[i].date, Repository.GetInstanse().GetTweets[i].text,
        //                      images[i]
        //        );
        //    }
        //}

        //public void CachingStart()
        //{
        //    db.DeleteTweetDB();
        //    SaveUserImage();
        //    SaveTweetDb();
        //}
    }
}
