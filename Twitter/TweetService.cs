﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TweetSharp;

namespace Twitter
{
    class TweetService
    {
        private long id;

        private const int count = 10;

        private TwitterService getTwitterService;

        public TweetService(TwitterService twitterService)
        {
            getTwitterService = twitterService;
        }

        public void GetTweets()
        {
            try
            {
                var tweets = getTwitterService.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions()).Take(count);
                id = tweets.Last().Id;

                foreach (var tweet in tweets)
                {
                    Tweet t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddTweet(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void GetHashTagTweets(string resultQuery)
        {
            try
            {
                var tweets = getTwitterService.Search(new SearchOptions() { Q = resultQuery }).Statuses.Take(count);

                id = tweets.Last().Id;

                foreach (var tweet in tweets)
                {
                    var t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddHashTagTweets(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void GetUserTweets(string resultQuery)
        {
            try
            {
                var tweets = getTwitterService.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions() { ScreenName = resultQuery }).Take(count);

                id = tweets.Last().Id;

                foreach (var tweet in tweets)
                {
                    var t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddScreenTweets(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetTweetsAutoPogruzchik()
        {
            try
            {
                var tweets = getTwitterService.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions { MaxId = id, Count = count }).Skip(1).Take(count);
                id = tweets.Last().Id;

                Repository.GetInstanse().GetTweets.Clear();

                foreach (var tweet in tweets)
                {
                    var t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddTweet(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetHashTagTweetsAutoPogruzchik(string resultQuery)
        {
            try
            {
                var tweets = getTwitterService.Search(new SearchOptions() { Q = resultQuery, Count = count, MaxId = id }).Statuses.Take(count).Skip(1).Take(count);

                id = tweets.Last().Id;

                Repository.GetInstanse().GetHashTagTweets.Clear();

                foreach (var tweet in tweets)
                {
                    var t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddHashTagTweets(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetUserTweetsAutoPogruzchik(string resultQuery)
        {
            try
            {
                var tweets = getTwitterService.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions() { ScreenName = resultQuery }).Skip(1).Take(count);

                id = tweets.Last().Id;

                Repository.GetInstanse().GetScreenTweets.Clear();

                foreach (var tweet in tweets)
                {
                    var t = new Tweet(tweet.Text, tweet.User.ProfileImageUrl, tweet.User.Name, tweet.User.ScreenName, tweet.CreatedDate);
                    Repository.GetInstanse().AddScreenTweets(t);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
