﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TweetSharp;

namespace Twitter
{
    /// <summary>
    /// Логика взаимодействия для TwitterInterface.xaml
    /// </summary>
    public partial class TwitterInterface : Window
    {
        private TwitterService getTwitterService;
        private UserService currentUser;
        private TweetService tweetService;
        //private CachingService cachingService;
        private string resultQuery;

        enum Priveleg
        {
            Default,
            User,
            HashTag
        }

        private Priveleg defaultPriveleg = Priveleg.Default;

        public TwitterInterface()
        {
            InitializeComponent();
            StartApplication();
            MessageBox.Show("Автор: Арсен Арустамян, 2016. https://github.com/arustamyan-developer");
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            tweetsItemsControl.Items.Clear();
            resultQuery = inputSearch.Text;

            if (resultQuery.StartsWith("@"))
            {
                Repository.GetInstanse().GetScreenTweets.Clear();
                tweetsItemsControl.Items.Clear();
                tweetService.GetUserTweets(resultQuery);
                AddItem(Repository.GetInstanse().GetScreenTweets);
                defaultPriveleg = Priveleg.User;
            } else {
                Repository.GetInstanse().GetHashTagTweets.Clear();
                tweetsItemsControl.Items.Clear();
                tweetService.GetHashTagTweets(resultQuery);
                AddItem(Repository.GetInstanse().GetHashTagTweets);
                defaultPriveleg = Priveleg.HashTag;
            }
        }

        private void homeButton_Click(object sender, RoutedEventArgs e)
        {
            tweetsItemsControl.Items.Clear();
            Repository.GetInstanse().GetTweets.Clear();
            tweetService.GetTweets();
            AddItem(Repository.GetInstanse().GetTweets);
            defaultPriveleg = Priveleg.Default;
            inputSearch.Text = "Введите #хештег или @имя";
        }

        private void buttonLoadTweets_Click(object sender, RoutedEventArgs e)
        {
            if (defaultPriveleg == Priveleg.Default)
            {
                tweetService.GetTweetsAutoPogruzchik();
                AddItem(Repository.GetInstanse().GetTweets);
            }
            else if (defaultPriveleg == Priveleg.HashTag)
            {
                tweetService.GetHashTagTweetsAutoPogruzchik(resultQuery);
                AddItem(Repository.GetInstanse().GetHashTagTweets);
            }
            else if (defaultPriveleg == Priveleg.User)
            {
                tweetService.GetUserTweetsAutoPogruzchik(resultQuery);
                AddItem(Repository.GetInstanse().GetScreenTweets);
            }

        }

        private void CreateUser()
        {
            currentUser = new UserService(getTwitterService);

            avatar.Source = new BitmapImage(new Uri(currentUser.GetUserImageUrl()));

            userName.Content = currentUser.GetUserName();

            userScreenName.Content = currentUser.GetUserScreenName();

            labelFollowersCount.Content = currentUser.GetUserFollowersCount();

            labelFriendsCount.Content = currentUser.GetUserFriendsCount();
        }

        private void AddItem(List<Tweet> t)
        {
            foreach (var tweet in t)
            {
                var currentDate = DateTime.Now;
                var createdDate = tweet.date;
                var result = currentDate.Subtract(createdDate);
                tweet.substractDate = SubstactDate(result);
                tweetsItemsControl.Items.Add(tweet);
            }
        }

        private void AddItem(List<TwitterTrend> t, ItemsControl control)
        {
            foreach (var tweet in t)
                control.Items.Add(tweet);
        }

        private void StartApplication()
        {
            getTwitterService = MainWindow.twitterService;
            CreateUser();
            var trendService = new TrendService(getTwitterService);
            trendService.GetTrends();
            AddItem(Repository.GetInstanse().GetTrends, trendsItems);
            tweetService = new TweetService(getTwitterService);
            tweetService.GetTweets();
            //cachingService = new CachingService();
            //cachingService.CachingStart();
            if (Repository.GetInstanse().GetTweets.Count > 0)
            {
                AddItem(Repository.GetInstanse().GetTweets);
            }
            //else {
            //    DataBaseService db = new DataBaseService();
            //    db.GetTweetDb();
            //    AddItem(Repository.GetInstanse().GetTweets);
            //}
        }

        private void screenHyperLinkClick(object sender, RoutedEventArgs e)
        {
            tweetsItemsControl.Items.Clear();
            Repository.GetInstanse().GetScreenTweets.Clear();
            Hyperlink link = (Hyperlink)sender;
            resultQuery = "@" + link.NavigateUri.ToString();
            inputSearch.Text = resultQuery;
            tweetService.GetUserTweets(resultQuery);
            AddItem(Repository.GetInstanse().GetScreenTweets);
            defaultPriveleg = Priveleg.User;
        }

        private void hashTagHyperlinkClick(object sender, RoutedEventArgs e)
        {
            tweetsItemsControl.Items.Clear();
            Hyperlink link = (Hyperlink)sender;
            resultQuery = link.NavigateUri.ToString();
            inputSearch.Text = resultQuery;
            SearchHyperlinkHashTag();
        }

        private void SearchHyperlinkHashTag()
        {
            Repository.GetInstanse().GetHashTagTweets.Clear();
            tweetService.GetHashTagTweets(resultQuery);
            AddItem(Repository.GetInstanse().GetHashTagTweets);
            defaultPriveleg = Priveleg.HashTag;
        }

        private string SubstactDate(TimeSpan d)
        {
            string result = "";

            if (d.Days > 0)
                result = d.Days + " день назад";
            else if (d.Hours > 3)
                result = (d.Hours - 3) + " ч. назад";
            else if (d.Minutes > 0)
                result = d.Minutes + " мин. назад";
            else if (d.Seconds > 0)
                result = d.Seconds + " сек. назад";
            return result;
        }
    }
}
