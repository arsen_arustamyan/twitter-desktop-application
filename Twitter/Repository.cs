﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;

namespace Twitter
{
    class Repository
    {
        private List<Tweet> tweets = new List<Tweet>();
        private List<Tweet> screenTweets = new List<Tweet>();
        private List<Tweet> hashTagTweets = new List<Tweet>();
        private List<TwitterTrend> trends = new List<TwitterTrend>();

        private static Repository _instanse;

        private Repository() { }

        public static Repository GetInstanse()
        {
            return _instanse ?? (_instanse = new Repository());
        }

        public void AddTweet(Tweet tweet)
        {
            tweets.Add(tweet);
        }

        public void AddScreenTweets(Tweet tweet)
        {
            screenTweets.Add(tweet);
        }

        public void AddHashTagTweets(Tweet tweet)
        {
            hashTagTweets.Add(tweet);
        }

        public void AddTrend(TwitterTrend trend)
        {
            trends.Add(trend);
        }

        public List<Tweet> GetTweets
        {
            get { return tweets; }
        }

        public List<Tweet> GetScreenTweets
        {
            get { return screenTweets; }
        }

        public List<Tweet> GetHashTagTweets
        {
            get { return hashTagTweets; }
        }

        public List<TwitterTrend> GetTrends
        {
            get { return trends; }
        }
    }
}
