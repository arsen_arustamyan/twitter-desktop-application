﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;

namespace Twitter
{
    class UserService : IUser
    {
        private TwitterService twitterService;

        public UserService(TwitterService twitterService)
        {
            this.twitterService = twitterService;
        }

        public string GetUserName()
        {
            return twitterService.GetUserProfile(new GetUserProfileOptions()).Name;
        }

        public string GetUserScreenName()
        {
            return "@" + twitterService.GetUserProfile(new GetUserProfileOptions()).ScreenName;
        }

        public string GetUserImageUrl()
        {
            return twitterService.GetUserProfile(new GetUserProfileOptions()).ProfileImageUrl;
        }

        public int GetUserFollowersCount()
        {
            return twitterService.GetUserProfile(new GetUserProfileOptions()).FollowersCount;
        }

        public int GetUserFriendsCount()
        {
            return twitterService.GetUserProfile(new GetUserProfileOptions()).FriendsCount;
        }

    }
}
