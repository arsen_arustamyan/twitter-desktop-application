﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter
{
    interface IUser
    {
        string GetUserName();

        string GetUserScreenName();

        string GetUserImageUrl();

        int GetUserFollowersCount();

        int GetUserFriendsCount();
    }
}
